<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package flavionevesweb
 */
?>

<!-- Footer section -->
<footer class="footer">
	<ul class="footer-social">
		<li class="fb"><a target="_blank" href="https://www.facebook.com/flavionevescarneiro"><i class="icon-facebook"></i></a></li>
		<li class="gplus"><a target="_blank" href="http://gplus.to/flavionevescarneiro"><i class="icon-google-plus"></i></a></li>
		<li class="linkedin"><a target="_blank" href="http://gplus.to/flavionevescarneiro"><i class="icon-linkedin"></i></a></li>
	</ul>

	<div class="footer-bottom">
		<p>&copy; 2014 Flavio Web Developer. Todos os direitos reservados.</p>
	</div>
</footer>
<!-- Footer section -->

<?php wp_footer(); ?>

</body>
</html>