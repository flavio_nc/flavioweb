<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package flavionevesweb
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="xnKi4qPpi0_EhKi-GC886Fp7iR8xChZVEmWkMpC5oXs" />
<title><?php wp_title(); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" /> 
<?php wp_head(); ?>
</head>

<body>
<div id="content">
<!-- Header wrap -->
<header id="header-wrap" class="row">
	<div class="responsive-logo"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" /></div>
    <div class="header">
		<div class="twelve columns">

			<!-- Logo -->
			<div class="five columns logo no-padding">
				<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" /></a>
			</div>
			<!-- Logo -->

			<!--  Nav menu -->
			<nav id="nav" class="seven columns no-padding ">
                <div class="imenu text-center">
                    <a><i class="icon-reorder"></i></a>
                </div>
				<ul class="navigation">
					<li class="current"><a href="#content">Home<span><i class="icon-home"></i></span></a></li>
					<li><a href="#services">Serviços<span><i class="icon-wrench"></i></span></a></li>
					<li><a href="#portfolio">Portfolio<span><i class="icon-folder-open"></i></span></a></li>
					<li><a href="#blog">Blog<span><i class="icon-pencil"></i></span></a></li>
					<li><a href="#contact">Contato<span><i class="icon-envelope"></i></span></a></li>
				</ul>
			</nav>
			<!--  Nav menu -->

		</div>
	</div>
	<div class="header-shadow"></div>
</header>
<!-- Header wrap -->
