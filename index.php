<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package flavionevesweb
 */

get_header(); ?>

<!-- Home section -->
<section id="home">
	<div class="row">
		<div class="twelve columns features-wrap">

			<div class="space80"></div>			
			<h3>Criação de soluções inteligentes para empresas</h3>
			<div class="line"></div>
            
            <div class="twelve column">
                <!--LayerSlider begin-->
                 <?php echo do_shortcode( '[masterslider id=1]' ) ?> 
            </div>
            <div class="twelve column text-center scroll-down">
                    <a href="#about"></a>
                    <span>Role para baixo</span>
            </div>
            <!--LayerSlider end-->
	   </div>		
</div>
</section>
<!-- About section -->
<section id="about">
	<div id class="team-main">
        <div class="row">
            <div class="twelve columns info-inner" itemscope itemtype="http://schema.org/Person">
                <div class="four columns team-info wow fadeInLeft">
                    <div class="team-inner">
                        <img class="team-thumb" src="<?php bloginfo('template_directory'); ?>/images/profile.jpg" alt="">
                        <h4>Flávio Neves<span>Web Developer</span></h4>
                        <div class="line2"></div>
                        <ul class="team-social">
                            <li class="fb"><a href="https://www.facebook.com/flavionevescarneiro" target="_blank"><i class="icon-facebook"></i></a></li>
                            <li class="gplus"><a href="http://gplus.to/flavionevescarneiro" target="_blank"><i class="icon-google-plus"></i></a></li>
                            <li class="linkedin"><a href="http://www.linkedin.com/pub/fl%C3%A1vio-neves-carneiro/40/980/865" target="_blank"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                	<div class="shadow1"></div>
                </div>
                <div class="eight columns description wow fadeInRight">
                    <h5 itemprop="name"><span>Flávio</span> Neves</h5>
                    <h6 itemprop="jobTitle">Web Developer</h6>
                    <p>Graduado em Engenharia de Computação pelo Instituto Federal de Ciência e Tecnologia do Ceará(IFCE), Flávio Neves Carneiro é natural de Fortaleza/CE, atualmente é Desenvolvedor Front-End e trabalha com desenvolvimento de Websites e Sistemas Web, com enfoque em Padrões da Web e na Experiência do Usuário (UX). Já trabalhou na Secretaria de Finanças do Município de Fortaleza(SEFIN), e na Câmara Municipal de Fortaleza como Desenvolvedor Web. É entusiasta por tecnologias e novas formas de inovação.</p>
                    <h5><span>Principais</span> Habilidades</h5>
                    
                    <ul class="skills-wrap">
                        <li data-wow-duration="500ms" class="wow flipInX">
                        	<span>WORDPRESS</span>
                        	<img src="<?php bloginfo('template_directory'); ?>/images/wordpress.png" />
                        </li>
                        <li data-wow-duration="1000ms" class="wow flipInX">
                        	<span>PHP</span>
                        	<img src="<?php bloginfo('template_directory'); ?>/images/php.png" />
                        </li>	
                        <li data-wow-duration="1500ms" class="wow flipInX">
                        	<span>JQUERY</span>
                        	<img src="<?php bloginfo('template_directory'); ?>/images/jquery.png" />
                        </li>
                        <li data-wow-duration="2000ms" class="wow flipInX">
                        	<span>HTML5</span>
                        	<img src="<?php bloginfo('template_directory'); ?>/images/html5.png" />
                        </li>
                        <li data-wow-duration="2500ms" class="wow flipInX">
                            <span>CSS3</span>
                            <img src="<?php bloginfo('template_directory'); ?>/images/css3.png" />
                        </li>	
                        <li data-wow-duration="3000ms" class="wow flipInX">
                            <span>PHOTOSHOP</span>
                            <img src="<?php bloginfo('template_directory'); ?>/images/ps.png" />
                        </li>	
                        <li data-wow-duration="3500ms" class="wow flipInX">
                            <span>SQL</span>
                            <img src="<?php bloginfo('template_directory'); ?>/images/sql.png" />
                        </li>
                    </ul>
                </div>
            <!-- Team wrap -->
            </div>
        </div>
	</div>
</section>

<!-- Services section -->
<section id="services">
	<div class="row">
		<div class="six columns centered text-center title wow fadeInDownBig">
  			<h1>Serviços</h1>
  			<h6>Principais serviços</h6>
		</div>
	</div>
	
    <div class="info-wrap">
		<div class="row">
				<div class="twelve columns info-inner wow bounceInLeft">
					<div class="six columns info-thumb">
						<img alt="" src="<?php bloginfo('template_directory'); ?>/images/internet.png">
					</div>
					<div class="six columns">
						<h5><span>Criação </span> de Sites</h5>
						<h6>Website development</h6>
						<p>A utilização da internet nos dias de hoje é cada vez maior, ela nos proporciona uma gama de possibilidades e facilidades, a principal delas é o acesso rápido e direto as informações que procuramos, oferecendo também a comodidade de busca-las sem sair de casa.</p>
					</div>
				</div>

				<div class="twelve columns info-inner wow bounceInRight">
					<div class="six columns info-thumb push-six">
						<img alt="" src="<?php bloginfo('template_directory'); ?>/images/Fotolia_47332328_Subscription_XXL.jpg">
					</div>
                    <div class="six columns pull-six">
						<h5><span>Sites</span> adaptáveis</h5>
						<h6>Responsive design</h6>
						<p>Um website definido como responsivo adapta sua exibição para uma ampla gama de dispositivos, como tablets e smartphones, melhorando a experiência de visualização, leitura e navegação.</p>
					</div>
					
				</div>
	
				<div class="twelve columns info-inner wow bounceInLeft">
					<div class="six columns info-thumb">
						<img alt="" src="<?php bloginfo('template_directory'); ?>/images/seo.png">
					</div>
					<div class="six columns">
						<h5><span>Otimização</span> de Sites</h5>
						<h6>Search Engine Optimization</h6>
						<p>Conjunto de estratégias com o objetivos de potencializar e impulsionar o posicionamento de seu site em resultados de páginas de buscas.</p>
					</div>
				</div>
                
                <div class="twelve columns info-inner wow bounceInRight">
					<div class="six columns info-thumb push-six">
						<img alt="" src="<?php bloginfo('template_directory'); ?>/images/marketing-social.png">
					</div>
                    <div class="six columns pull-six">
						<h5><span>Marketing</span> nas Redes Sociais</h5>
						<h6>Social media marketing</h6>
						<p>Um plano de marketing de redes sociais é essencial para seu site, é nele que há um fortalecimento da imagem do seu negócio na internet, através de uma comunicação direta e objetiva com seu público.</p>
					</div>
					
				</div>
                
                <div class="twelve columns info-inner wow bounceInLeft">
					<div class="six columns info-thumb">
						<img alt="" src="<?php bloginfo('template_directory'); ?>/images/remote-acess.jpg">
					</div>
					<div class="six columns">
						<h5><span>Assessoria</span> remota</h5>
						<h6>Remote Access Service</h6>
						<p>Serviço de solução imediata de suas dúvidas e problemas com seu computador.</p>
					</div>
				</div>
                
			</div>
		</div>

	<!-- ends -->
	
</section>
<!-- Services section -->

<!-- Parallax section -->
<section class="parallax">
    <div class="parallax1"></div>
    <div class="pattern"></div>
    <div class="row">
        <div class="twelve columns clients centered wow fadeInUp">
            <h4>Principais Clientes</h4>
            <div class="line2"></div>
            <div class="space40"></div>
            <div class="carousel">
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-cmfor.png" alt=""/></div>
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-preventiva.png" alt=""/></div>
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-acasadostoldos.png" alt=""/></div>
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-casanova.png" alt=""/></div>
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-bussola.png" alt=""/></div>
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-wda.png" alt=""/></div>
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-falaceara.png" alt=""/></div>
                <div><img src="<?php bloginfo('template_directory'); ?>/images/logo-dicionariofortaleza.png" alt=""/></div>
            </div>
        </div>
    </div>
</section>
<!-- Parallax section -->

<!-- Portfolio section -->
<section id="portfolio">
	<div class="row">
		<div class="six columns centered text-center info-inner wow fadeInDownBig">
  			<h1>Portfolio</h1>
  			<h6>Principais projetos desenvolvidos</h6>
		</div>
	</div>
    <div class="row">
		<div class="twelve columns wow fadeInUpBig">
			<div class="space80"></div>
		     <!-- Start Filter -->
			<ul class="folio-filter" data-option-key="filter">
				<li><a class="selected" href="#filter" data-option-value="*"><span></span>Todos</a></li>
				<li><a href="#filter" data-option-value=".designs"><span></span>Designs</a></li>
				<li><a href="#filter" data-option-value=".websites"><span></span>WebSites</a></li>
				<li><a href="#filter" data-option-value=".sistemas"><span></span>Sistemas</a></li>
		    	</ul>
			<!-- End Filter -->
				    	
		    	<div id="folio" class="isotope ">
                
                <?php

        $args = array(
            'post_type' => array( 'portfolio' ),
            'posts_per_page' => -1,
            'orderby' => 'rand'

         );
            
        $wp_query = new WP_Query( $args );
        
        ?>
        <?php if( $wp_query->have_posts() ) : ?>

			<?php while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
            
				<?php
                    
                $terms = wp_get_post_terms(get_the_ID(),'tipo');

                ?>
                
                <div class="folio-item four columns isotope-item <?php echo $terms[0]->slug; ?>">
                    <div class="folio-inner">
                        <div class="portfolio_thumb">
                            <?php the_post_thumbnail(); ?>
                            <div class="portfolio-inner" >
                                <div class="zoom-inner">
                                    <ul class="link-info">
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full'); ?>
                                        <li class="zoom"><a title="<?php echo simple_fields_value('titulo'); ?>" href="<?php echo $image[0]; ?>" class="prettyPhoto" ><i class="icon-search"></i></a></li>
                                        <?php if(!empty(simple_fields_value('url'))) : ?>
                                        <li class="link"><a title="Acessar" href="<?php echo simple_fields_value('url'); ?>" target="_blank"><i class="icon-external-link"></i></a></li>
                                        <?php endif; ?>
                                    </ul>		
                                </div>
                            </div>
                        </div>
    
                        <div class="folio-info">
                            <h4><a target="_blank" href="http://wp.cmfor.ce.gov.br"><?php echo simple_fields_value('titulo'); ?></a></h4>
                            <div class="line6"></div>
                            <p><?php echo simple_fields_value('descricao'); ?></p>
                        </div>
                    </div>
                </div>

			<?php endwhile; ?>
            
        <?php endif; ?>
						
			</div>
		</div>
	</div>
</section>
<!-- Portfolio section -->

<!-- Parallax section -2 -->
<div class="inner-wrap2">
		<div class="row">
			<div class="twelve columns wow bounceIn">
				<h4>Faça um <span>orçamento</span> agora.</h4>
				<div class="button1">
					<a href="#contact">Fazer orçamento</a>
				</div>
			</div>
		</div>
</div>
<!-- Parallax section -2 -->

<!-- Blog Section -->
<section id="blog">
	<div class="row">
		<div class="six columns centered text-center info-inner wow fadeInDownBig">
  			<h1>Blog</h1>
  			<h6>Últimas notícias do mundo da tecnologia</h6>
		</div>
	</div>
    <div class="row">
		<div class="twelve columns wow fadeInUpBig">
			<div class="space90"></div>
			<div class="blog">
				<?php
				
				global $paged;
				
				$args = array(
				    'orderby' => 'post_date',
				    'paged' => $paged
				);
				
				$query = new WP_Query($args);
				
				?>
                <?php if ($query->have_posts()) : ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php $field = simple_fields_fieldgroup("opcoes"); ?>
					<div class="blog-post">
						<div class="blog-inner">
						<div class="blog-thumb">
							<a target="_blank" href="<?php echo $field['link']; ?>"><?php the_post_thumbnail('large'); ?></a>
						</div>
						<div class="blog-meta">
							<a href="#"><?php the_category(', '); ?></a> | <?php the_time('j \d\e F \d\e Y'); ?>
						</div>		
						<div class="blog-info">
							<a target="_blank" href="<?php echo $field['link']; ?>"><h4><?php the_title(); ?></h4></a>
                            <a target="_blank" href="<?php echo $field['link']; ?>"><p><?php the_content(); ?></p></a>
						</div>
						<div class="blog-share">
							<i class="icon-retweet"></i> via -
                            <img src="http://www.google.com/s2/favicons?domain=<?php echo $field['link']; ?>" /><span><?php echo $field['nome_site']; ?></span>
						</div>
						</div>
					</div>
                     
                    <?php endwhile; ?>
                    <?php else : ?>
                     
                        [O que fazer ser não há posts?]
                     
                <?php endif; ?>
					
			</div>
		</div>
	</div>
</section>
<!-- Blog Section -->

<!-- Parallax section -3 -->
<div class="inner-wrap3">
	<div class="row">
	  <div class="eight columns centered">
		<div class="six columns mcmp-info">
			<h4>Assine a Newsletter<span>do blog</span></h4>
			<p>Receba as principais notícias do blog no seu email!</p>
		</div>

		<div class="six columns">
			<img src="<?php bloginfo('template_directory'); ?>/images/flat-pencil.png" alt=""/>
		</div>

		<div class="twelve columns">
			<!--BEGIN subscribe -->
			<div id="subscribe-wrap">
				<div id="subscribes">
					<form action="#" method="post">
						<input type="email" value="" name="EMAIL" class="required email">
						<input type="submit" value="Assinar"/>
					</form>
				</div>
			</div>
			<!--END subscribe -->
		</div>
	  </div>


<!-- Subscribe section -->
	</div>
</div>
<!-- Parallax section -3 -->

<!-- Contact section -->
<section id="contact">
	<div class="row">
        <div class="six columns centered text-center">
				<h2>Contato</h2>
		</div>
		<div class="space50"></div>
		<div class="six columns">
			<!--<div class="map-wrap">
				<div class="map">
					<div class="gmap">
						<div id="map_addresses" class="map"></div>
					</div>
					<div class="shadow4"></div>
				</div>
			</div>

			<!-- Contact info -->
			<ul class="contact-info">
				<li><i class="ico icon-phone"></i>(85)8812.9065</li>
                <li><i class="ico icon-mobile-phone"></i>(85)9686.2890</li>
				<li><i class="ico icon-envelope-alt"></i>flavionevescarneiro@gmail.com</li>
                <li><i class="ico icon-facebook"></i>/flavionevesweb</li>
			</ul>
		</div>

		<div class="six columns">
			<!-- Contact form -->
			<?php echo do_shortcode('[contact-form-7 id="61" title="Formulário de contacto 1"]'); ?>
			<script></script>
		</div>
	</div>
</section>
<!-- Contact section -->

<?php get_footer(); ?>
