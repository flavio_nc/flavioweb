<?php
/**
 * flavionevesweb functions and definitions
 *
 * @package flavionevesweb
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'flavionevesweb_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function flavionevesweb_setup() {
	
	show_admin_bar(false);
	
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on flavionevesweb, use a find and replace
	 * to change 'flavionevesweb' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'flavionevesweb', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
    
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'flavionevesweb' ),
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'flavionevesweb_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // flavionevesweb_setup
add_action( 'after_setup_theme', 'flavionevesweb_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function flavionevesweb_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'flavionevesweb' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'flavionevesweb_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function load_scripts() {
    
    if( !is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js');
    }
    
	wp_deregister_script('custom');
    wp_register_script('custom', get_template_directory_uri().'/js/custom.js', array('jquery'), '', true);
	
	wp_enqueue_script('queryloader', get_template_directory_uri().'/js/jquery.queryloader2.min.js', array('jquery'), '', true);
    wp_enqueue_script('slicknav', get_template_directory_uri().'/js/jquery.slicknav.min.js', array('jquery'),'', true);
    wp_enqueue_script('localscroll', get_template_directory_uri().'/js/jquery.localscroll-1.2.7-min.js', array('jquery'),'', true);
	wp_enqueue_script('slick-carousel', get_template_directory_uri().'/js/slick.min.js', array('jquery'),'', true);
    wp_enqueue_script('wow', get_template_directory_uri().'/js/wow.min.js', array('jquery'),'', true);
    wp_enqueue_script('masonry', get_template_directory_uri().'/js/masonry.pkgd.min.js', array('jquery'), '', true);
    wp_enqueue_script('imagesloaded', get_template_directory_uri().'/js/imagesloaded.js', array('jquery'), '', true);
    wp_enqueue_script('detectmobilebrowser', get_template_directory_uri().'/js/detectmobilebrowser.js', array('jquery'), '', true);
    wp_enqueue_script('sticky', get_template_directory_uri().'/js/jquery.sticky.js', array('jquery'), '', true);
    wp_enqueue_script('prettyphoto', get_template_directory_uri().'/js/jquery.prettyphoto.js', array('jquery'), '', true);
    wp_enqueue_script('parallax', get_template_directory_uri().'/js/jquery.parallax-1.1.3.js', array('jquery'), '1.1.3', true);
    wp_enqueue_script('isotope', get_template_directory_uri().'/js/jquery.isotope.min.js', array('jquery'), '', true);
    wp_enqueue_script('scrollTo', get_template_directory_uri().'/js/jquery.scrollTo.js', array('jquery'), '', true);
    wp_enqueue_script('nav', get_template_directory_uri().'/js/jquery.nav.js', array('jquery'), '', true);
    wp_enqueue_script('google-maps', 'http://maps.google.com/maps/api/js?sensor=false', array('jquery'), '', true);
    wp_enqueue_script('gmap', get_template_directory_uri().'/js/jquery.gmap.js', array('jquery'), '', true);

    wp_enqueue_script('easing', get_template_directory_uri().'/js/jquery.easing.1.3.js', array('jquery'), '1.3', true);

    wp_enqueue_script('camera', get_template_directory_uri().'/js/camera.min.js', array('jquery'), '', true);
    wp_enqueue_script('jquery-placeholder', get_template_directory_uri().'/js/jquery.placeholder.min.js', array('jquery'), '', true);
    wp_enqueue_script('waypoints', get_template_directory_uri().'/js/waypoints.js', array('jquery'), '', true);
    wp_enqueue_script('snap', get_template_directory_uri().'/js/snap.js', array('jquery'), '', true);
	
	wp_enqueue_script('custom');
    
   
	
}
add_action( 'wp_enqueue_scripts', 'load_scripts' );

function load_styles() {
    
    wp_register_style( 'style', get_template_directory_uri() . '/css/style.css', 'all' );
    wp_register_style( 'reset', get_template_directory_uri() . '/css/reset.css', 'all' );
    wp_register_style( 'foundation', get_template_directory_uri() . '/css/foundation.css', array(), 'all' );
	wp_register_style( 'slicknav', get_template_directory_uri() . '/css/slicknav.css', array(), 'all' );
    wp_register_style( 'ico', get_template_directory_uri() . '/css/ico.css', array(), 'all' );
	wp_register_style( 'slick-carousel', get_template_directory_uri() . '/css/slick.css', array(), 'all' );
    wp_register_style( 'isotope', get_template_directory_uri() . '/css/isotope.css', array(), 'all' );
    wp_register_style( 'prettyphoto', get_template_directory_uri() . '/css/prettyphoto.css', array(), 'all' );
    wp_register_style( 'camera', get_template_directory_uri() . '/css/camera.css', array(), 'all' );
    wp_register_style( 'animate', get_template_directory_uri() . '/css/animate.css', array(), 'all' );
    wp_register_style( 'animations', get_template_directory_uri() . '/css/animations.css', array(), 'all' );
    wp_register_style( 'layerslider', get_template_directory_uri() . '/css/layerslider.css', array(), 'all' );
    wp_register_style( 'layerslider-skins', get_template_directory_uri() . '/css/layerslider-skins/minimal/skin.css', array(), 'all' );
    wp_register_style( 'snap', get_template_directory_uri() . '/css/snap.css', array(), 'all' );
    
    wp_enqueue_style( 'reset' );
    
    wp_enqueue_style( 'foundation' );
	
	wp_enqueue_style( 'slick-carousel' );
	
    wp_enqueue_style( 'slicknav' );
    
	wp_enqueue_style( 'ico' );

	wp_enqueue_style( 'isotope' );

	wp_enqueue_style( 'prettyphoto' );

	wp_enqueue_style( 'camera' );

	wp_enqueue_style( 'animate' );

	wp_enqueue_style( 'animations' );

	wp_enqueue_style( 'layerslider' );

	wp_enqueue_style( 'layerslider-skins' );
    
	wp_enqueue_style( 'snap' );
    
    wp_enqueue_style( 'open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,800');
    
    wp_enqueue_style( 'open-sans-condensed', 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700');
    
    wp_enqueue_style( 'oswald', 'http://fonts.googleapis.com/css?family=Oswald:400,300');
    
    wp_enqueue_style( 'alef', 'http://fonts.googleapis.com/css?family=Alef:400,700');
	
	wp_enqueue_style( 'style' );
     
}
add_action( 'wp_enqueue_scripts', 'load_styles' );

add_action('init', 'cpt_portfolio');
function cpt_portfolio() {
    register_post_type('portfolio', array(
    'label' => 'Portfolio',
    'description' => '',
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'map_meta_cap' => true,
    'hierarchical' => false,
    'rewrite' => array('slug' => 'portfolio', 'with_front' => true),
    'query_var' => true,
    'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
    'labels' => array (
      'name' => 'Portfolio',
      'singular_name' => 'Portfolio',
      'menu_name' => 'Portfolio',
      'add_new' => 'Add Portfolio',
      'add_new_item' => 'Add New Portfolio',
      'edit' => 'Edit',
      'edit_item' => 'Edit Portfolio',
      'new_item' => 'New Portfolio',
      'view' => 'View Portfolio',
      'view_item' => 'View Portfolio',
      'search_items' => 'Search Garotas',
      'not_found' => 'No Portfolio Found',
      'not_found_in_trash' => 'No Portfolio Found in Trash',
      'parent' => 'Parent Portfolio',
    )
    )); 
}

function themes_taxonomy() {  
    register_taxonomy(  
        'tipo',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'portfolio',        //post type name
        array(  
            'hierarchical' => true,  
            'label' => 'Tipo',  //Display name
            'query_var' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'themes', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before 
            )
        )  
    );  
}  
add_action( 'init', 'themes_taxonomy');

function debug($var, $exit = false){
	echo '<pre>';
	print_r($var);
	echo '</pre>';
	echo ($exit)? exit : '';
}

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
