jQuery(document).ready(function ($) {
	
	$("body").queryLoader2({
        backgroundColor: '#317DBE',
        barColor: '#fff',
        barHeight: 5,
        minimumTime: 2000,
        percentage: true
    });
	
	var wow = new WOW(
	  {
		boxClass:     'wow',      // animated element css class (default is wow)
		animateClass: 'animated', // animation css class (default is animated)
		offset:       0,          // distance to the element when triggering the animation (default is 0)
		mobile:       true,       // trigger animations on mobile devices (default is true)
		live:         true        // act on asynchronously loaded content (default is true)
	  }
	);
	wow.init();
	
	$('.navigation').slicknav({
		prependTo : '#header-wrap'
	});
	
	$('.slicknav_nav a').click(function(){
		$('.slicknav_nav').slideUp();
	});
	
	$('.carousel').slick({
		arrows: false,
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000
	});
	
	//Oculta selecionados
    $('.team-info, .description, .info-wrap .info-inner ').addClass('hidden');
	
	// initialize Masonry after all images have loaded  
	$('.blog').imagesLoaded( function() {
	    $(this).masonry({
	        itemSelector: '.blog-post',
	        columnWidth: 50
    	});
	});
    
    $(window).resize(function(){
        windowWidth = $(window).width(); //retrieve current window width
        windowHeight = $(window).height(); //retrieve current window height
        if(windowWidth < 483){
            $('.logo').addClass('mobile-three');
            $('nav').addClass('mobile-one');
        } else {
            $('.logo').removeClass('mobile-three');
            $('nav').removeClass('mobile-one');
        }
    });
    
	$('#contactForm').submit(function(){
				
				$('.error').remove();
				erro = false;
				
				var txtNome = $("input#txtNome").val(); 
				if (txtNome == "") {  
					$('<span class="error animated bounceIn">*Informe um nome</span>').insertBefore('input#txtNome');
					erro = true;
				}  
				var txtEmail = $("input#txtEmail").val();  
				if (txtEmail == "") {  
					$('<span class="error animated bounceIn">*Informe um email</span>').insertBefore('input#txtEmail');
					erro = true;
				}
				var txtTel = $("input#txtAssunto").val();  
				if (txtTel == "") {  
					$('<span class="error animated bounceIn">*Informe um telefone</span>').insertBefore('input#txtAssunto');
					erro = true;
				}  
				var txtMsg = $("#txtMsg").val();  
					if (txtMsg == "") {  
					$('<span class="error animated bounceIn">*Informe uma mensagem</span>').insertBefore('#txtMsg');
					erro = true;

			} 
			
			if(erro) return false;
			
			var dataString = 'txtNome='+ txtNome + '&txtEmail=' + txtEmail + '&txtAssunto=' + txtAssunto + '&txtMsg=' + txtMsg;
			console.log(dataString);
			
			$.ajax({  
				type: "POST",  
				url: "/wp-content/themes/flavionevesweb/inc/enviaForm.php",  
				data: dataString,
				beforeSend: function () {
						$("#result").html('<img src="loding.gif" /> Now loding...');
				},
				success: function() { 
					$("<span>Sua mensagem foi recebida com sucesso!</span>").insertAfter('.btn-submit');
					$('#form-faleconosco').siblings('span').delay(5000).fadeOut('slow');
					$('#form-faleconosco input, #form-faleconosco textarea').val('');
				}  
			});  
			return false;
		});
		
			$(document).ajaxStart(function() {
				$('.ajax-load').removeClass('hidden');
			}).ajaxStop(function () {
				$('.ajax-load').addClass('hidden');
		});
    
    
	//======== ANIMATIONS ==================//

    


    /*if (!Modernizr.svg) {
        $(".logo img").attr("src", "images/logo.png");
    }*/

    //======== LOCAL SCROLL =============//
    $('.navigation li, .rnavigation li, .slicknav_menu li').localScroll({ duration: 1000 });
    $('.button1').localScroll({ duration: 1000 });
    $('.scroll-down').localScroll({ duration: 1000});

    //========= SCROLL NAV ==============//
    $('.navigation, .rnavigation').onePageNav({ filter: ':not(.external)' });

    //======== STICKY HEADER =============//
   $("#header-wrap").sticky({topSpacing:0});

    //============ PARALLAX =============//

    $('.parallax1').parallax("10%", 0.1);
    $('.inner-wrap2').parallax("10%", 0.1);
    $('.inner-wrap3').parallax("10%", 0.1, true);

    //=========== LIGHTBOX =============//
    $("a[class^='prettyPhoto']").prettyPhoto({ theme: 'pp_default' });

    //======== CAMERA SLIDER ============//
    jQuery('#camera_wrap_1').camera({
        height: '58%',
        pagination: false,
        hover: false,
        loader: 'none',
        thumbnails: false
    });

    //======== TOP CONTENT LAZYLOAD ============//
    $(window).load(function () {
        jQuery(".top-inner").show();
    });

    //=========== ISOTOPE =============//
    var $container = $('#folio');
    $container.imagesLoaded( function() {
        $(this).isotope({
            itemSelector: '.folio-item'
        });
    });
    var $optionSets = $('#portfolio .folio-filter'),
	$optionLinks = $optionSets.find('a');
    $optionLinks.click(function () {
        var $this = $(this);
        // don't proceed if already selected
        if ($this.hasClass('selected')) {
            return false;
        }
        var $optionSet = $this.parents('.folio-filter');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
	key = $optionSet.attr('data-option-key'),
	value = $this.attr('data-option-value');

        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[key] = value;
        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            changeLayoutMode($this, options);
        } else {
            // otherwise, apply new options
            $container.isotope(options);
        }
        return false;
    });

});